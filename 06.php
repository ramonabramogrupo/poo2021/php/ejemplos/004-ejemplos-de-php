<?php

/**
 * Muestra dos numeros que le paso como argumentos
 * @param float $numero1 el primer numero a mostrar
 * @param float $numero2 El segundo numero a mostrar 
*/
function mostrar($numero1,$numero2=10){
    echo "numero1: {$numero1}<br>";
    echo "numero2: {$numero2}<br>";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        mostrar(12);
        mostrar(13,5);
        ?>
    </body>
</html>
