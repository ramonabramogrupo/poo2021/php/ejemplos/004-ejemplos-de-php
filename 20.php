<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // funcion anonima 
        (function(){
            $a=12;
            $b=10;
            echo $a+$b;
        })();
        
        $numero1=10;
        $numero2=100;
        $n=10;
        
        // funcion anonima pasando argumentos 
        (function ($a,$b) use($n){
            echo $a+$b+$n;
        })($numero1,$numero2);
        
        // funcion anonima como closure
        $a=function($valores){
            return array_sum($valores);
        };
        
        echo $a([2,3,4]);
                      
        
        // funciones variables
        $b="suma";
        function suma(){
            echo "sumando";
        }
        
        $b();
        
        
        
        
        
        ?>
    </body>
</html>
