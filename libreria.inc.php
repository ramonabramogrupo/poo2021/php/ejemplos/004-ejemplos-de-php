<?php

function suma($numeros) {
    $resultado = 0;
    foreach ($numeros as $valor) {
        $resultado += $valor;
    }
    return $resultado;
}

function producto($numeros) {
    $resultado = 1;
    foreach ($numeros as $valor) {
        $resultado *= $valor;
    }
    return $resultado;
}

function promedio($numeros) {
    $media = 0; // almacenar la media
    $suma = 0; // almacenar la suma
    $cantidad = count($numeros); // elementos del array
    foreach ($numeros as $valor) {
        $suma = $suma + $valor;
        //$suma+=$valor;
    }
    $media = $suma / $cantidad;
    return $media;
}
