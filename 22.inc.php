<?php
/**
 * Funcion que retorna todos los elementos de un directorio como textos
 * @param string $handle Ruta relativa a la carpeta a estudiar
 * @return array Array con todos los elementos de la carpeta a estudiar
 */  

    function leerDirectorio($handle = "..") {
            $handle=opendir($handle);
            while (false !== ($archivo = readdir($handle))) {
                $archivos[] = strtolower($archivo);
            }
            closedir($handle);
            sort($archivos);
            return $archivos;
        }
        
/**
 * Funcion que retorna todos los elementos de un directorio como textos
 * @param string $handle Ruta relativa a la carpeta a estudiar
 * @return array Array con todos los elementos de la carpeta menos el . y ..
 */
        
    function leerDirectorioSin($handle = "..") {
            $handle=opendir($handle);
            while (false !== ($archivo = readdir($handle))) {
                if($archivo!="." && $archivo!=".."){
                    $archivos[] = strtolower($archivo);
                }
            }
            closedir($handle);
            sort($archivos);
            return $archivos;
        }
