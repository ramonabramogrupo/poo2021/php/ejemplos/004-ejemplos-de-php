<?php

/**
 * muestra dos numeros incrementadoles una unidad
 * @param int $numero1
 * @param int $numero2
 */
function mostrar($numero1,$numero2=10){
    $numero1++;
    $numero2++;
    echo "numero1: {$numero1}<br>";
    echo "numero2: {$numero2}<br>";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $n1=10;
        $n2=50;
        mostrar($n1);
        mostrar($n1,$n2);
        echo "n1: {$n1}<br>";
        echo "n2: {$n2}<br>";
        ?>
    </body>
</html>
