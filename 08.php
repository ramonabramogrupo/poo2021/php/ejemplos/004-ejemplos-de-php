<?php 
function suma(...$numeros){
    var_dump($numeros);
    $salida=0;
    foreach ($numeros as $n){
        $salida=$salida+$n;
    }
    return $salida;
}

function sumaAntigua(){
    $numeros= func_get_args();
    var_dump($numeros);
    $salida=0;
    foreach ($numeros as $n){
        $salida=$salida+$n;
    }
    return $salida;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            echo suma(34,2,3,1);
        ?>
        <div><?= suma(1,3,2) ?></div>
        <?php
            echo sumaAntigua(34,2,3,1);
        ?>
        <div><?= sumaAntigua(1,3,2) ?></div>
    </body>
</html>
