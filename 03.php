<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // creo una funcion sin argumentos 
        
        /**
         * Mostrar mensaje en pantalla
         * @param string $mensaje Mensaje a mostrar
         */
        function mostrar($mensaje){
            echo "<div>$mensaje</div>";
        }
        
        // llamo a la funcion 3 veces
        mostrar("hola");
        mostrar("clase");
        mostrar("hola clase");
        
        ?>
    </body>
</html>